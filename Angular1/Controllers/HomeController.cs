﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Angular1.Models;
using Microsoft.AspNetCore.Cors;

namespace Angular1.Controllers
{
    public class HomeController : Controller
    {
        IRecordRepository db;
        public HomeController(IRecordRepository repository)
        {
            db = repository;
        }
        [HttpGet]
        public string Index()
        {
            return "Index";
        }
        [HttpGet]
        public IActionResult GetRecords(string mask)
        {
            mask = mask == null ? "*" : mask;
            if(mask == "*")
            {
                return Json(db.GetList(), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            }
            else
            {
                return Json(db.GetList(mask), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            }
        }
        
        [HttpPost]
        public IActionResult AddNewRecord([FromBody]Record record)
        {
            if(record != null && !string.IsNullOrEmpty(record.Name) && !string.IsNullOrEmpty(record.Phone))
            {
                db.Create(record);
                return Json("success");
            }
            else
            {
                return Json("error");
            }

        }
    }
}