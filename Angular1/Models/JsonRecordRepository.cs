﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using RecordList = System.Collections.Generic.List<Angular1.Models.Record>;


namespace Angular1.Models
{
    public class JsonRecordRepository : IRecordRepository
    {
        private RecordList records { get; set; }
        private static object syncRoot = new object();

        public JsonRecordRepository ()
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(RecordList));

            using (FileStream fs = new FileStream("phonebook.json", FileMode.OpenOrCreate))
            {
                if (new FileInfo("phonebook.json").Length != 0)
                    records = (RecordList)jsonFormatter.ReadObject(fs);
            }
        }

        public IEnumerable<Record> GetList(string mask)
        {
            var lowerMask = mask.ToLower(); 
            return records?.Where(rec => rec.Name.ToLower().Contains(lowerMask)).ToList(); 
        }

        public Record Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Create(Record item)
        {
            lock(syncRoot)
            {
                records.Add(item);
                Task task = new Task(Save);
                task.Start();
            }
        }

        public void Update(Record item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(RecordList));

            using (FileStream fs = new FileStream("phonebook.json", FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(fs, records);
            }
        }
    }
}
