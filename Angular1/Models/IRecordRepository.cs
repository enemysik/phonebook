﻿using System;
using System.Collections.Generic;

namespace Angular1.Models
{
    public interface IRecordRepository
    {
        IEnumerable<Record> GetList(string mask = "*");
        Record Get(int id); //id остались, но реализацию их я не делал
        void Create(Record item);
        void Update(Record item);
        void Delete(int id);
        void Save();
    }
}
