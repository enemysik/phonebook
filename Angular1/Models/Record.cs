﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace Angular1.Models
{
    [Serializable]
    [DataContract]
    public class Record
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Phone { get; set; }

        public Record(string name, string phone)
        {
            this.Name = name;
            this.Phone = phone;
        }
        public Record() { }
    }
}
