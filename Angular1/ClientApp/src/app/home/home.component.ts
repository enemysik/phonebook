import { Component, OnInit } from '@angular/core';
import { Record } from '../models/record.model';
import { HttpService } from '../services/http.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [HttpService]
})
export class HomeComponent implements OnInit {

  records: Record[] = [];
  showEmptyInscription: boolean = false;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.http.fetchRecords('*').subscribe((data: Record[]) => {
      this.records = data;
    })
  }

  toggle() {
    this.showEmptyInscription = !this.showEmptyInscription;
  }

  submit(form: NgForm) {
    this.http.fetchRecords(form.value.name).subscribe((data: Record[]) => {
      this.records = data;
      if (data.length == 0)
        this.showEmptyInscription = true;
      else
        this.showEmptyInscription = false;
    })
     }
}
