import { Component } from '@angular/core'
import { NgForm } from '@angular/forms'
import { HttpService } from '../services/http.service'
import { Record } from '../models/record.model'

@Component({
  selector: 'app-add-new-record',
  templateUrl: './add-new-record.component.html',
  styleUrls: ['add-new-record.component.css'],
  providers: [HttpService]
})
export class AddNewRecordComponent {

  showSuccess: boolean = false;
  showWarning: boolean = false;

  constructor(private http: HttpService) { }

  toggleSuccess() {
    this.showSuccess = !this.showSuccess;
  }
  toggleWarning() {
    this.showWarning = !this.showWarning;
  }
  
  submit(form: NgForm) {
    let record: Record = new Record(form.value.name, form.value.phone);
    this.http.addNewRecord(record).subscribe(
      data => {
        if (data == "success")
        {
          this.showSuccess = true;
          form.reset();
        }
        else
        {
          this.showWarning = true;
        }
      }
    );
  }
}
