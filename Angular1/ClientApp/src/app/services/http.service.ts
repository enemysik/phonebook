import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Record } from '../models/record.model';
import { error } from 'protractor';

@Injectable()
export class HttpService {
  constructor(private http: HttpClient) { }

  addNewRecord(record: Record) {
    const body = { Name: record.name, Phone: record.phone };
    return this.http.post('/Home/AddNewRecord', body);
  }

  fetchRecords(mask: string) {
    const params = new HttpParams().set('mask', mask);
    return this.http.get('/Home/GetRecords', { params });
  }
}
